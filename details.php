<?php 
	require_once('initialize.php');

	$breadcrumd = ['Inicio' => "index.php", 'Tarea 1' => ""];

	$precio = 350;
	$descuentoProd = 0;
	$cantidad = 0;
	$descuentoIva = 0;
	$total = 0;
	$show = false;


	if(isset($_GET['token'])){
		$show = true;
		$cantidad = $_GET['cantidad'];
		$totalSinDescuento = $precio * $cantidad;
		$descuentoProd = $cantidad <= 3 ? $totalSinDescuento * 0.05 : 0.15 * $totalSinDescuento ;
		$descuentoIva = 0.30 * $totalSinDescuento ;
		$total = $totalSinDescuento - $descuentoIva - $descuentoProd ;
	}


	echo view("header", ["page_title" => "Tarea 1", "breadcrumd" => $breadcrumd] );
 ?>


<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="card card-primary">
			  <div class="card-header"> Game Cube</div>
			  <div class="card-body">
				  	<p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/GameCube-Set.jpg/1024px-GameCube-Set.jpg"> </p>

				  	<p>The GameCube[b] is a home video game console released by Nintendo in Japan and North America in 2001 and Europe and Australia in 2002. The sixth generation console is the successor to the Nintendo 64 and competed with Sony's PlayStation 2 and Microsoft's Xbox. </p>

				  	<h3>Q 350.00</h3>

				  	
					<form action="details.php" method="get">

                        <p class="control">
                        	<input type="hidden" name="cantidad" value="<?php echo $_GET['cantidad']; ?>">
                        	<input type="hidden" name="token" value="valor">
                            <input class="button is-info" type="submit" value="Aplicar descuentos">
                        </p>
                  	</form>

			  </div>
			</div>
		</div>

		<?php if($show) { ?>
		<div class="col-md-6">
			<div class="card card-primary">
			  <div class="card-header"> Detalle</div>
			  <div class="card-body">
			  	<p>Cantidad a comprar: <?php echo $cantidad; ?></p> 
			  	<p>Total sin descuento: <?php echo $totalSinDescuento; ?></p> 
			  	<hr>
			  	<p>Descuento: <?php echo $descuentoProd; ?></p> 
			  	<p>Descuento Iva: <?php echo $descuentoIva; ?></p> 
			  	<hr>

			  	<p>Total a pagar: <?php echo $total; ?></p> 
			  
			  </div>
			</div>
		</div>
		<?php } ?>
	</div>
</div>
  
<?php echo view("footer", []); ?>