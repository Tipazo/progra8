<?php 
	require_once('../initialize.php');

    $edit = false;
    if(isset($_GET['id'])){
        $edit = true;
        $subject_set = getById('subjects', $_GET['id']);
        $subject = mysqli_fetch_assoc($subject_set);
    }

    if(isset($_POST['option']))
    {
        if($_POST['option'] == "editar"){
            updateById('subjects', $_GET['id'], $_POST);

            $subject_set = getById('subjects', $_GET['id']);
            $subject = mysqli_fetch_assoc($subject_set);
        }

        if($_POST['option'] == "nuevo")
        {
            createItem('subjects', $_POST);
        }
    }

    $breadcrumd = ['Inicio' => "index.php", "Temas" => "temas.php", ($edit == true ? "Mostrar" : "Nuevo") => ""];

	echo view("admin_header", ["page_title" => "Paginas", "breadcrumd" => $breadcrumd] );
 ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h3 class="top-space">Tema</h3>

            <?php if( is_null($subject) ) { ?>
            <h4>No existe registro a mostrar</h4>
            <?php } ?>

                <form action="" action="" method="POST">
                  <div class="form-group">
                    <label for="text">Nombre Menu</label>
                    <input type="text" class="form-control" name="menu_name" value="<?php echo $edit == true ?  $subject['menu_name']: "";  ?>" id="menu_name">
                  </div>
                  <div class="form-group">
                    <label for="text">Posicion</label>
                    <input type="number" step="1" min="0" max="10" class="form-control" name="position" value="<?php echo $edit == true ?  $subject['position']: "";  ?>" id="position">
                  </div>
                  <div class="form-group">
                    <label for="text">Visible</label>
                    <input type="number" step="1" min="0" max="10" class="form-control" name="visible" value="<?php echo $edit == true ?  $subject['visible']: "";  ?>" id="visible">
                  </div>

                  <input type="hidden" name="option" value="<?php echo $edit == true ?  "editar": "nuevo"; ?>">
                  <button type="submit" class="btn btn-default">Submit</button>

                </form>

        </div>
    </div>
</div>
    
<?php echo view("footer", []); ?>



