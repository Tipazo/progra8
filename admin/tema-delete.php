<?php 
    require_once('../initialize.php');

    $breadcrumd = ['Inicio' => "index.php", "Temas" => "temas.php", "Eliminar" => ""];

    $subject_set = getById('subjects', $_GET['id']);

    $subject = mysqli_fetch_assoc($subject_set);


    if(isset($_POST['id']))
    {
        deleteItem('subjects', $_POST['id']);
    }

    echo view("admin_header", ["page_title" => "Paginas", "breadcrumd" => $breadcrumd] );
 ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h3 class="top-space">Tema</h3>

                <?php if( !is_null($subject) ) { ?>
                <h4>Seguro de eliminar <?php echo $subject['menu_name']; ?></h4>

                <form action="" action="" method="POST">
                
                  <input type="hidden" name="id" value="<?php echo $subject['id']; ?>">
                  <button type="submit" class="btn btn-danger">Delete</button>

                </form>

                <?php } ?>

                <?php if( is_null($subject) ) { ?>
                <h4>No existe registro a mostrar</h4>
                <?php } ?>
        </div>
    </div>
</div>
    
<?php echo view("footer", []); ?>



