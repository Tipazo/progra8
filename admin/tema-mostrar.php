<?php 
	require_once('../initialize.php');

    $breadcrumd = ['Inicio' => "index.php", "Temas" => "temas.php", "Mostrar" => ""];

	$subject_set = getById('subjects', $_GET['id']);

    $subject = mysqli_fetch_assoc($subject_set);

	echo view("admin_header", ["page_title" => "Paginas", "breadcrumd" => $breadcrumd] );
 ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">

            <h3 class="top-space">Tema</h3>

            <?php if( !is_null($subject) ) { ?>
            <p>  <strong> Nombre menu</strong>: <?php echo $subject['menu_name']; ?></p>
            <p>  <strong> Posicion </strong>: <?php echo $subject['position']; ?></p>
            <p>  <strong> Visible </strong>: <?php echo $subject['visible']; ?></p>

            <?php } ?>
            <?php if( is_null($subject) ) { ?>
            <h4>No existe registro a mostrar</h4>
            <?php } ?>
        </div>
    </div>
</div>
    
<?php echo view("footer", []); ?>



