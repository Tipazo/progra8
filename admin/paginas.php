<?php 
	require_once('../initialize.php');

	$breadcrumd = ['Inicio' => ""];

	$subject_set = find_all_pages();


	echo view("admin_header", ["page_title" => "Paginas", "breadcrumd" => $breadcrumd] );
 ?>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <section>
            <h3 class="top-space">Administración de Temas</h3>
            <section class="top-space">
                <a class="btn btn-success"
                    href=" <?php echo url_for('/admin/subjects/new.php');?>">
                <i class="fas fa-pencil-alt"></i> Crear nuevo tema
                </a>
                <p class="top-space"></p>
            </section>
            <div class="table-responsive">
                <table class="table">
                    <tr>
                        <th class="center-text">ID</th>
                        <th class="center-text">Posición</th>
                        <th class="center-text">Visible</th>
                        <th>Nombre</th>
                        <th>Opciones</th>
                    </tr>
                    <?php while($subject = mysqli_fetch_assoc($subject_set)) { ?>
                    <tr>
                        <td class="center-text"><?php echo $subject['id']; ?></td>
                        <td class="center-text"><?php echo $subject['position']; ?></td>
                        <td class="center-text"><?php echo $subject['visible'] == 1 ? 'Sí':'No';?></td>
                        <td><?php echo $subject['menu_name'] ;?></td>
                        <td><?php echo $subject['menu_name'] ;?></td>
                    </tr>
                    <?php } ?>
                </table>
            </div>
        </div>
    </div>
</div>
    
<?php echo view("footer", []); ?>



