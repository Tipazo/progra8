<?php 
	require_once('initialize.php');

	session_start();

	if(isset($_SESSION["id"])) {
		$player = mysqli_fetch_assoc(getById('players', $_SESSION["id"]));
		$lastLevel = mysqli_fetch_assoc(getPlayerLabel('player_levels', $_SESSION["id"], $_SESSION["nivel"]));

		$_SESSION["id"] = $player['id'];
		$_SESSION["nivel"] = $lastLevel['nivel'];

	}
	if(isset($_GET['id'])) {

		$player = mysqli_fetch_assoc(getById('players', $_GET['id']));
		$lastLevel = mysqli_fetch_assoc(getLastLevelPlayer('player_levels', $_GET['id']));

		$_SESSION["id"] = $player['id'];
		$_SESSION["nivel"] = $lastLevel['nivel'];
	}

	if(isset($_POST['token']) && $_POST["token"] != "" && isset($_POST['aumentar']) ){

		$nivel = $_POST['nivel'] + 1;
		$ataque = $_POST['ataque'] * 1.5;
		$defensa = $lastLevel['defensa'] * 1.5;
		$rapidez = $lastLevel['rapidez'] * 1.5;
		$poder_total = round(($ataque + ($defensa / 2 )) * ($rapidez / 100));

		$playerResult2 = insert_player_levels([
			'nivel' => $nivel,
			'ataque' => $ataque,
			'defensa' => $defensa,
			'rapidez' => $rapidez,
			'poder_total' => $poder_total,
			'player_id' => $_POST['player_id'],
		]);

	    if($playerResult2 === true){
			$lastLevel = mysqli_fetch_assoc(getPlayerLabel('player_levels', $_SESSION["id"], $nivel));
			$_SESSION["nivel"] = $nivel;

	    } else {
	        $errors = $playerResult2;
	    }
	}

	if(isset($_POST['token2'])){
		if($_POST['reducir'] > 0){
			$lastLevel = mysqli_fetch_assoc(getPlayerLabel('player_levels', $_SESSION["id"], $_POST['reducir']));
			$_SESSION["nivel"] = $_POST['reducir'];
		}
	}

	$breadcrumd = ['Inicio' => "index.php", 'Crear Personaje' => "parcial2.php", 'Jugar' => ""];

	echo view("mobile_header", ["page_title" => "Crear Personaje", "breadcrumd" => $breadcrumd] );
 ?>

<style type="text/css">
		.tipo { text-transform:  uppercase; font-weight: bold;  }

		.puntos { display: block; }
		.nivel { background: #3361D3 !important; }
		.ataque { background: #b5b726 !important; }
		.defensa { background: #d63f15 }
		.rapidez { background: #269399 }
		.poder_total { background: #37a301 }
		.ui-bar-a { 
			color:  #fff !important; 
			text-shadow: 2px 1px #555; 
			text-align: center;
		}
</style>


<div data-role="page" id="home" data-url="home" tabindex="1" class="ui-page ui-page-theme-a ui-page-active">

    <div data-role="header">
        <h1><?php echo $player['name']; ?></h1>
        <a data-transition="flip" rel="external" data-theme="a" class="ui-btn ui-icon-arrow-r " href="parcial-2.php">
            Regresar
        </a>
    </div><!-- /header -->
    <div role="main" class="ui-content">
		<div class="ui-grid-b">
		    <div class="ui-block-a">
		        <div class="ui-bar ui-bar-a nivel" style="height:40px">
		            Nivel <span class="puntos"><?php echo $lastLevel['nivel']; ?></span>
		        </div>
		    </div>
		    <div class="ui-block-b">
		        <div class="ui-bar ui-bar-a ataque" style="height:40px">
		            Ataque <span class="puntos"><?php echo $lastLevel['ataque']; ?></span>
		        </div>
		    </div>
		    <div class="ui-block-c">
		        <div class="ui-bar ui-bar-a defensa" style="height:40px">
		            Defensa <span class="puntos"><?php echo $lastLevel['defensa']; ?></span>
		        </div>
		    </div>
		</div>

		<div class="ui-grid-b">
		    <div class="ui-block-a">
		        <div class="ui-bar ui-bar-a rapidez" style="height:40px">
		            Rápidez <span class="puntos"><?php echo $lastLevel['rapidez']; ?></span>
		        </div>
		    </div>
		    <div class="ui-block-b">
		        <div class="ui-bar ui-bar-a poder_total" style="height:40px">
		            Poder <span class="puntos"><?php echo $lastLevel['poder_total']; ?></span>
		        </div>
		    </div>
		    <div class="ui-block-c">
		        <div class="ui-bar ui-bar-a" style="height:40px">

		        </div>
		    </div>
		</div>
        <?php echo display_errors($errors); ?>
		<fieldset class="ui-grid-a">
		    <div class="ui-block-a">
		        <form id="formParcial1" method="POST" data-ajax="false">
		            <input type="hidden" name="player_id" value="<?php echo $player['id']; ?>">
		            <input type="hidden" name="nivel" value="<?php echo $lastLevel['nivel']; ?>">
		            <input type="hidden" name="ataque" value="<?php echo $lastLevel['ataque']; ?>">
		            <input type="hidden" name="defensa" value="<?php echo $lastLevel['defensa']; ?>">
		            <input type="hidden" name="rapidez" value="<?php echo $lastLevel['rapidez']; ?>">
		            <input type="hidden" name="poder_total" value="<?php echo $lastLevel['poder_total']; ?>">
		            <input type="hidden" name="aumentar" value="<?php echo $lastLevel['poder_total']; ?>">
		            <input type="hidden" name="token" value="<?php echo md5("holamundo"); ?>">
		            <button type="submit" class="submit1">Subir nivel</button>	
		        </form>
		    </div>
		    <div class="ui-block-b">
		        <form id="formParcial1" method="POST" data-ajax="false">
		            <input type="hidden" name="reducir" value="<?php echo $_SESSION["nivel"] - 1; ?>">
		            <input type="hidden" name="token2" value="<?php echo md5("holamundo"); ?>">
		            <button type="submit" class="submit1">Bajar nivel</button>	
		        </form>
		    </div>
		</fieldset>
		<div align="center"> 
			<img src="public/imas/robot.svg" width="<?php echo $lastLevel['nivel'] * 60;?>px">
		</div>
    </div><!-- /content -->

    <div data-role="footer">
        <h4>Udeo 2018 - Eduardo Tipaz</h4>
    </div><!-- /footer -->
</div><!-- /page -->

<?php echo view("mobile_footer", []); ?>