<?php 
	require_once('initialize.php');

	$lenguages = [ "php", "Java", "Ruby", "Vue.js", "C++"];
	$paises = [ "Guatemala", "Cuba", "Serbia", "Suiza", "Brazil", "Japon"];
	$problemas = [ "Login", "Camara", "Gps", "Teclado", "Refresh"];

	$breadcrumd = [
		'Inicio' => "index.php", 'Tarea 2' => ""];

	$subproblemas = [
		"Login" => ["No funciona boton submit", "No permite numeros"],
		"Camara" => ["No graba videos", "No tiene opción de zoom"],
		"Gps" => ["No muestra el current location", "Al refrescar no varia la locación"],
		"Teclado" => ["No permite cambiar de lenguage", "No muestra signos"],
		"Refresh" => ["Refresca cada 10 minutos", "Al refrescar lleva al home"],
	];

	echo view("header", ["page_title" => "Tarea 2 Formularios", "breadcrumd" => $breadcrumd] );
 ?>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-primary">
			  <div class="card-header"> Customer care form</div>
			  <div class="card-body">
				<form id="formIssues" action="" method="post">

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						        <label for="name">Nombre:</label>
						        <input type="text" class="form-control" name="nombre" id="name">
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						        <label for="email">Email address:</label>
						        <input type="email" class="form-control" name="email" id="email">
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						        <label for="Plataforma">Plataforma:</label>
						        <select class="form-control" name="plataforma" id="Plataforma">
						            <option value="Windows">Windows</option>
						            <option value="Mac">Mac</option>
						            <option value="iOS">iOS</option>
						            <option value="Android"> Android</option>
						        </select>
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						        <label for="lenguage">Lenguage:</label>
						        <select class="form-control" name="lenguage" id="lenguage">
						        	<?php foreach ($lenguages as $key1 => $lenguages) { ?>
						            	<option value="<?php echo $lenguages; ?>"><?php echo $lenguages; ?></option>
						        	<?php } ?>            
						        </select>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						        <label for="lenguage">Pais:</label>
						        <select class="form-control" name="pais" id="pais">
						        	<?php foreach ($paises as $key2 => $pais) { ?>
						            	<option value="<?php echo $pais; ?>"><?php echo $pais; ?></option>
						        	<?php } ?>            
						        </select>
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						        <label for="lenguage">Topic:</label>
						        <select class="form-control" name="topic" id="topic">
						            <option value="">-- Seleccionar Topic -- </option>
						        	<?php foreach ($problemas as $key3 => $problema) { ?>
						            	<option value="<?php echo $problema; ?>"><?php echo $problema; ?></option>
						        	<?php } ?>            
						        </select>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
						    <div class="form-group">
						        <label for="lenguage">Issue:</label>
						        <select class="form-control" name="issue" id="issue">
						            <option value="">-- Seleccionar Issue -- </option>
						        </select>
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						        <label for="lenguage">Version: </label>
								<div class="radio">
								  <label><input type="radio" name="version" value="v.001" checked> v.001</label>
								</div>
								<div class="radio">
								  <label><input type="radio" name="version" value="v.002" > v.002</label>
								</div>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="token" value="<?php echo md5("holamundo"); ?>">
				    		<button type="submit" class="btn btn-default">Submit</button>	
						</div>
					</div>
				</form>
			  </div>
			</div>
		</div>

		<p>&nbsp;</p>
		<?php if (isset($_POST['token']) && !is_null($_POST['token'])) {

			$errorCount = 0;

			foreach ($_POST as $key => $valuePost) {
				if($valuePost == ""){
					$errorCount++;
				}
			}

		 ?>
		 	<?php if($errorCount==0) { ?>
				<div class="col-md-12">
					<div class="card card-primary">
						<div class="card-header"> Detalles del formulario</div>
						<div class="card-body">
							<table>
							    <tbody>
							      <tr>
							        <td>Nombre: </td>
							        <td><?php echo $_POST['nombre'] ?></td>
							      </tr>
							      <tr>
							        <td>Email: </td>
							        <td><?php echo $_POST['email'] ?></td>
							      </tr>
							      <tr>
							        <td>Plataforma: </td>
							        <td><?php echo $_POST['plataforma'] ?></td>
							      </tr>
							      <tr>
							        <td>Lenguage: </td>
							        <td><?php echo $_POST['lenguage'] ?></td>
							      </tr>
							      <tr>
							        <td>Pais: </td>
							        <td><?php echo $_POST['pais'] ?></td>
							      </tr>
							      <tr>
							        <td>Topic: </td>
							        <td><?php echo $_POST['topic'] ?></td>
							      </tr>
							      <tr>
							        <td>Issue: </td>
							        <td><?php echo $_POST['issue'] ?></td>
							      </tr>
							      <tr>
							        <td>Version: </td>
							        <td><?php echo $_POST['version'] ?></td>
							      </tr>
							    </tbody>
							</table>
						</div>
					</div>
				</div>
		 	<?php } ?>

		 	<?php if($errorCount>0) { ?>
			 	<div class="col-md-12">
					<div class="alert alert-danger">
					  <strong>Danger!</strong> Error en el formulario
					</div>
				</div>
		 	<?php } ?>

		<?php } ?>

	</div>
</div>
   
<script type="text/javascript">
	var subproblemas = <?php echo json_encode($subproblemas); ?>
</script>

<?php echo view("footer", []); ?>