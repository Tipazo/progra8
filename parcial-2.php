<?php 
    require_once('initialize.php');

    session_start();

    $breadcrumd = ['Inicio' => "index.php", 'Crear Personaje' => ""];
    $poderInicial = (100 + (500 / 2)) * 2.5;
    // (mitad de la defensa + ataque) * decimal de la rapidez

    $players = getAll('players');

    if(isset($_POST['token']) && $_POST["token"] != "") 
    {
        $subject = [];
        $subject['name'] = $_POST['name'];

        $values = [
            'nivel' => $_POST['nivel'],
            'ataque' => $_POST['ataque'],
            'defensa' => $_POST['defensa'],
            'rapidez' => $_POST['rapidez'],
            'poder_total' => $_POST['poder_total']
        ];

        $playerResult = insert_player($subject, $values);
        if($playerResult === true){

            $player1 = getByName('players', $_POST['name']);

            if(!is_null($player1)) {
                $player = mysqli_fetch_assoc($player1);

                $values['player_id'] = $player['id'];
                $playerResult2 = insert_player_levels($values);

                if($playerResult2 === true) {
                    $_SESSION["id"] = $player['id'];
                    $_SESSION["nivel"] = $_POST['nivel'];
                    redirect_to("play.php");
                }
            }
        } else {
            $errors = $playerResult;
        }
    }

    echo view("mobile_header", ["page_title" => "Crear Personaje", "breadcrumd" => $breadcrumd] );
 ?>
<div data-role="page" id="home" data-url="home" tabindex="1" class="ui-page ui-page-theme-a ui-page-active">

    <div data-role="header">
        <h1>Crear bot</h1>
        <a data-transition="flip" data-theme="a" class="ui-btn ui-icon-arrow-r " href="#list">
            Lista de bots
        </a>
    </div><!-- /header -->
    <div role="main" class="ui-content">
        <?php echo display_errors($errors); ?>
        <form method="post" id="formParcial1" data-ajax="false">
            <label for="name">Nombre : </label>
            <input type="text" class="form-control" name="name" id="name">
            <label for="nivel">Nivel: </label>
            <input type="number"  min="0" max="100" class="form-control" name="nivel" id="nivel" value="1" readonly>
            <label for="ataque">Ataque: </label>
            <input type="number"  min="0" max="100" class="form-control" name="ataque" id="ataque" value="100" readonly>
            <label for="defensa">Defensa: </label>
            <input type="number"  min="0" max="1000" class="form-control" name="defensa" id="defensa" value="500" readonly>
            <label for="rapidez">Rápidez: </label>
            <input type="number"  min="0" max="1000" class="form-control" name="rapidez" id="rapidez" value="250" readonly>
            <label for="poder_total">Poder Total: </label>
            <input type="number"  min="0" max="100" class="form-control" name="poder_total" id="poder_total" value="<?php echo $poderInicial; ?>" readonly>
            <input type="hidden" name="token" value="<?php echo md5("holamundo"); ?>">
            <button type="submit" value="Submit" data-theme="b" class="btn btn-default">Guardar</button>   
        </form>
    </div><!-- /content -->

    <div data-role="footer">
        <h4>Udeo 2018 - Eduardo Tipaz</h4>
    </div><!-- /footer -->
</div><!-- /page -->


<div data-role="page" id="list" data-url="list" tabindex="0" class="ui-page">
    <div data-role="header">
        <h1>Lista de bots</h1>
        <a data-transition="flip" href="#home" data-theme="a" class="ui-btn ui-icon-arrow-r ">
            Crear bot
        </a>
    </div><!-- /header -->

    <div role="main" class="ui-content">
        <ul data-role="listview">
            <?php while($subject = mysqli_fetch_assoc($players)) { ?>
                <li><a rel="external" href="play.php?id=<?php echo $subject['id']; ?>"><?php echo $subject['name']; ?></a></li>
            <?php } ?>
        </ul>
    </div><!-- /content -->

    <div data-role="footer">
        <h4>Udeo 2018 - Eduardo Tipaz</h4>
    </div><!-- /footer -->
</div><!-- /page -->

<?php echo view("mobile_footer", []); ?>