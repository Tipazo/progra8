<?php 
	require_once('cms/initialize.php');

	session_start();

	$breadcrumd = ['Inicio' => "index.php", 'Control de notas' => "parcial1.php", 'Nota' => ''];

	$show = false;
	if(isset($_SESSION["notas"][$_GET["notaid"]])) {
		$nota = $_SESSION["notas"][$_GET["notaid"]];
		$show = true;
	}

	echo view("header", ["page_title" => "Control de notas", "breadcrumd" => $breadcrumd] );
 ?>


<div class="container">
	<div class="row">

	 	<?php if($show) { ?>
			<div class="col-md-12">
				<div class="card card-primary">
					<div class="card-header"> Notas del alumno: <strong><?php echo $nota['nombre']; ?></strong></div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
								<th>Materia</th>
								<th>Primers Trimestre</th>
								<th>Segudo Trimestre</th>
								<th>Tercer Trimestre</th>
								<th>Cuarto Trimestre</th>
								<th>Promedio</th>
								<th>Resultado</th>
							</tr>
							</thead>
						    <tbody id="tableResultado">
					    		<tr>
					    			<td><?php echo $nota['curso']; ?></td>
					    			<td><?php echo $nota['primerTrimestre']; ?></td>
					    			<td><?php echo $nota['segundoTrimestre']; ?></td>
					    			<td><?php echo $nota['tercerTrimestre']; ?></td>
					    			<td><?php echo $nota['cuartoTrimestre']; ?></td>
					    			<td><?php echo $nota['promedio']; ?></td>
					    			<td class="<?php echo $nota['promedio'] >= 70 ? "btn-success" : "btn-danger" ?>">
					    				<?php echo $nota['promedio'] >= 70 ? "Aprobado" : "Reprobado" ?>
					    			</td>
					    		</tr>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	 	<?php }  else { ?>
		 	<div class="col-md-12">
				<div class="alert alert-danger">
				  <strong>Danger!</strong> No existe nota de referencia para mostrar
				</div>
			</div>
	 	<?php }?>

	</div>
</div>
   

<?php echo view("footer", []); ?>