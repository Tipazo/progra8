<!DOCTYPE html>
<html class="ui-mobile">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Eduardo Tipaz - udeo</title>
        <!-- CONFIGURACIÓN de la WEB APP -->
        <meta name="viewport" content="user-scalable=no,width=device-width,initial-scale=1">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta name="format-detection" content="telephone=no">
        <link rel="apple-touch-icon" sizes="120x120" href="apple-touch-icon-120x120.png">
        <link rel="apple-touch-startup-image" media="(device-width: 320px) and (-webkit-device-pixel-ratio: 2)" href="apple-touch-startup-image-640x920.png">
        <!-- JQUERY MOBILE -->
        <link rel="stylesheet" type="text/css" href="public/mobile/jquery.mobile-1.4.5.min.css">
        <script type="text/javascript" src="public/mobile/jquery-1.10.2.min.js"></script>
        <!-- CDN -->
        <!-- <script src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="crossorigin="anonymous"></script> -->
        <script type="text/javascript" src="public/mobile/jquery.mobile-1.4.5.min.js"></script>
        <!-- CUSTOM FILES -->
        <link rel="stylesheet" type="text/css" href="public/mobile/styles.css">
        <link rel="stylesheet" type="text/css" href="public/mobile/animate.css">
    </head>
    <body class="ui-mobile-viewport ui-overlay-a" cz-shortcut-listen="true">