<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>THE CMS: <?php echo  $page_title;?></title>
    <link rel='stylesheet' href="public/css/style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
</head>
     <body>
          <header class="jumbotron center-text">
               <h1 class="display-1 bold">
                    <i class="fas fa-newspaper animated fliplnY"></i>
                    THE CMS
               </h1>
               <h3>Bienvenido a la zona de administracion!</h3>
               <p>
                    Utilice esta zona para administrar..
                    <br> Tambien puede utilizar esta seccion para..
               </p>
          </header>
          <div class="container">
              <div class="row">
                  <div class="col-md-12">
                      <nav aria-label="breadcrumb">
                        <ol class="breadcrumb">
                          <?php foreach($breadcrumd as $key => $value) { ?>
                              <?php echo $value == '' ? '<li class="breadcrumb-item active" aria-current="page">'.$key.'</li>' : '<li class="breadcrumb-item "><a href="'.$value.'">'.$key.'</a></li>'?>
                          <?php } ?>
                        </ol>
                      </nav>
                  </div>
              </div>
          </div>

          
          <nav class="center-text">
               <ul>
                    <li>
                         <a href="<?php echo url_for('/index.php');?>" class="btn btn-info>">
                              <i class="fas fa-globe-americas"></i>
                              WebSite
                         </a>
                    </li>
                    <li>
                         <a href="<?php echo url_for('/admin/index.php');?>" class="btn btn-info">
                              <i class="far fa-addresss-card"></i>
                              Menu
                         </a>
                    </li>
               </ul>
          </nav>
