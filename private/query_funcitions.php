<?php
function getAll($tableName){
	global $db;

	$query = "SELECT * FROM $tableName ORDER BY id ASC";
	$result_set = mysqli_query($db,$query);

	return $result_set;
}

function getById($tableName, $id){
	global $db;

	$query = "SELECT * FROM $tableName where id = $id limit 1";

	$result_set = mysqli_query($db,$query);

	return $result_set;
}

function updateById($tableName, $id, $data) {

	unset($data['option']);
	$txtQuery = "SET ";
	$count = count($data);
	$index = 1;
	foreach ($data as $key1 => $value) {
		$txtQuery .= $key1 . "=" . "'". $value ."'" . ($index < $count ? "," :"");
		$index++;
	}

	global $db;
	$query = "UPDATE $tableName $txtQuery WHERE id = $id";

	$result_set = mysqli_query($db,$query);

	return $result_set;
}

function createItem ($tableName, $data)
{
	unset($data['option']);
	$txtCols = "";
	$txtValues = "";
	$count = count($data);
	$index = 1;
	foreach ($data as $key1 => $value) {
		$txtCols .= $key1 . ($index < $count ? "," :"");
		$txtValues .= "'". $value ."'".($index < $count ? "," :"");
		$index++;
	}

	global $db;
	$query = "INSERT INTO $tableName ($txtCols) VALUES ($txtValues)";

	$result_set = mysqli_query($db,$query);

	return $result_set;
}

function deleteItem($tableName, $id){

	global $db;

	$query = "DELETE FROM $tableName WHERE id = $id LIMIT 1";
	$result_set = mysqli_query($db,$query);

	return $result_set;
}


function getByName($tableName, $name) {
	global $db;
	$query = "SELECT * FROM $tableName  where name = '$name' limit 1";

	$result_set = mysqli_query($db,$query);

	return $result_set;	
}


function getPlayerLabel($tableName, $id, $nivel) {
	global $db;
	$query = "SELECT * FROM $tableName  where player_id = '$id' AND nivel = '$nivel'  limit 1";

	$result_set = mysqli_query($db,$query);

	return $result_set;	
}

function getLastLevelPlayer($tableName, $id) 
{
	global $db;
	$query = "SELECT * FROM $tableName  where player_id = '$id' order by id desc limit  1";

	$result_set = mysqli_query($db,$query);

	return $result_set;	
}



// NEW
function validate_player($subject){
    $errors = [];

    if(is_blank($subject['name'])){
        $errors[] = "nombre no puede estar en blanco.";
    } elseif(!has_length($subject['name'], ["min" => 5, "max" => 255] )){
        $errors[] = "Nombre player debe contener entre 5 y 255 caracteres";
    }

    return $errors;
}

function insert_player($subject, $levels)
{
    $errors = validate_player($subject);
    if(!empty($errors)){
        return $errors;
    }

    $errors = validate_lever_player($levels);
    if(!empty($errors)){
        return $errors;
    }

    global $db;

    $query = "INSERT INTO players";
    $query .= "(name)";
    $query .= "VALUES(";
    $query .= "'" . db_escape($db, $subject['name']) . "')";

    $result_set = mysqli_query($db,$query);

    if ($result_set) {
        return true;
    }else{
        $errors[] = mysqli_error($db);
        db_disconnect($db);
        return $errors;
        exit;
    }
}


function validate_lever_player($subject){
    $errors = [];

    $ataque = (int) $subject['ataque'];
    if ($ataque <= 0){
        $errors[] = "Ataque debe ser mayor a cero";
    }

    $defensa = (int) $subject['defensa'];
    if ($defensa <= 0){
        $errors[] = "Defensa debe ser mayor a cero";
    }

    $rapidez = (int) $subject['rapidez'];
    if ($rapidez <= 0){
        $errors[] = "Rapidez debe ser mayor a cero";
    }

    $poder_total = (int) $subject['poder_total'];
    if ($poder_total <= 0){
        $errors[] = "Poder total debe ser mayor a cero";
    }

    return $errors;
}


function getAllLevelsbyPlayer($id){
	global $db;

	$query = "SELECT * FROM player_levels  where player_id = '$id' order by nivel";
	$result_set = mysqli_query($db,$query);

	return $result_set;
}


function insert_player_levels($subject)
    {
        $errors = validate_lever_player($subject);
        if(!empty($errors)){
            return $errors;
        }

        global $db;

        $query = "INSERT INTO player_levels ";
        $query .= "(player_id, defensa, nivel, ataque, rapidez, poder_total)";
        $query .= "VALUES(";
        $query .= "'" . db_escape($db, $subject['player_id']) . "',";
        $query .= "'" . db_escape($db, $subject['defensa'] ). "', ";
        $query .= "'" . db_escape($db, $subject['nivel'] ). "', ";
        $query .= "'" . db_escape($db, $subject['ataque'] ). "', ";
        $query .= "'" . db_escape($db, $subject['rapidez'] ). "', ";
        $query .= "'" . db_escape($db, $subject['poder_total']) . "')";

        $result_set = mysqli_query($db,$query);

        if ($result_set) {
            return true;
        }else{
            //echo mysqli_error($db);
            //db_disconnect($db);
            //exit;
            $errors[] = mysqli_error($db);
            db_disconnect($db);
            return $errors;
            exit;
        }
    }



// OLD

function find_all_subjects(){
	global $db;

	$query = "SELECT * FROM subjects ORDER BY position ASC";
	$result_set = mysqli_query($db,$query);

	return $result_set;
}


function find_all_pages(){
	global $db;

	$query = "SELECT * FROM pages ORDER BY position ASC";
	$result_set = mysqli_query($db,$query);
	
	return $result_set;
}