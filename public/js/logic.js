(function(){

    document.getElementById("topic").onchange = function() {showTopic()};
    
    function showTopic() {
        var opt = subproblemas[topic.value];
        document.getElementById("issue").innerHTML = "<option value=''> -- Seleccion un Issue -- </option>";
        opt.forEach(function(element, index) {
            document.getElementById("issue").innerHTML += "<option value='"+ element +"'> "+ element +" </option>";
        });
    }


    document.addEventListener('DOMContentLoaded', function(){
        document.getElementById('formIssues').addEventListener('submit', sendDetails);

        function sendDetails(event){
            event.preventDefault();

            var name = document.getElementById("name");

            if(name.value == "") {
                alert("Favor ingrese un nombre");
                name.focus();
                return false;
            }

            var correo = document.getElementById("email");

            if(correo.value == "") {
                alert("Favor ingrese un correo");
                correo.focus();
                return false;
            }

            if(!correo.value.match(/^[a-z0-9_\-]+(\.[_a-z0-9\-]+)*@([_a-z0-9\-]+\.)+([a-z]{2}|aero|arpa|biz|com|coop|edu|gov|info|int|jobs|mil|museum|name|nato|net|org|pro|travel)$/i) ){
                alert("Favor ingresar un correo valido");
                correo.focus();
                return false;
            }

            var topic = document.getElementById("topic");

            if(topic.value == "") {
                alert("Favor seleccione un topic");
                topic.focus();
                return false;
            }

            var issue = document.getElementById("issue");

            if(issue.value == "") {
                alert("Favor seleccione un Issue");
                issue.focus();
                return false;
            }

            document.getElementById("formIssues").submit();
        }
    });
})();