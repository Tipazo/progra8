
window.onload = function(){
}

function getDate_(a){

    var today = new Date();
    var dd = today.getDate()+a;
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();
    var minutes = today.getMinutes();
    var hours = today.getHours();
    var seconds = today.getSeconds();

    if(dd<10) {
        dd = '0'+dd
    } 

    if(mm<10) {
        mm = '0'+mm
    } 

    return yyyy + '-' + mm + '-' + dd;
}


function makeid() {
  var text = "";
  var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

  for (var i = 0; i < 5; i++)
    text += possible.charAt(Math.floor(Math.random() * possible.length));

  return text;
}


var subtotal = 0;
var iva = 0;
var gastosEnvio = 0;
var tipoPago = 0;
var total = 0;

var selectedPago = false;
var selectedEnvio = false;

var porcentajeEnvio = 0;
var porcentajePago = 0;

var nombreCliente = '';
var nitCliente = '';

var fechaEntrega = getDate_(9);


function agregarItem(element, counterPhafer, price) {

	var counterPadre = Number(document.getElementById("count_"+counterPhafer).innerHTML);
	if(counterPadre > 0) {
		document.getElementById("count_"+counterPhafer).innerHTML = counterPadre - 1;

		var counter = Number(document.getElementById("items_count").innerHTML) + 1;
		document.getElementById("items_count").innerHTML = counter

		var element = document.getElementById(element).innerHTML;
		var idEliminar = makeid();
		var parentRemove = makeid();

		var elementDetalles = document.getElementById("detalle_compra");
			var template1 = '<li id="'+parentRemove+'"><a href="#">'+element+'</a><a href="#'+idEliminar+'"data-rel="popup"data-position-to="window"data-transition="pop">Purchasealbum</a></li>'
			elementDetalles.innerHTML += template1;


		var elemenEliminaciones = document.getElementById("detalle_eliminaciones");
			var parentRemove2 = "'"+parentRemove+"'";
			var counterPhafer2 = "'"+counterPhafer+"'";
			var idEliminar2 = "'"+idEliminar+"'";

			var template2 = '<div data-role="popup" id="'+idEliminar+'" data-theme="a" data-overlay-theme="b" class="ui-content" style="max-width:340px; padding-bottom:2em;"> <h3>Eliminar producto</h3>  '+element+'  <a href="index.html" onclick="deleteItem('+parentRemove2+','+idEliminar2+','+counterPhafer2+','+price+')" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-b ui-icon-check ui-btn-icon-left ui-btn-inline ui-mini">Si, eliminar</a> <a href="index.html" data-rel="back" class="ui-shadow ui-btn ui-corner-all ui-btn-inline ui-mini">Cancelar</a> </div>';
			elemenEliminaciones.innerHTML += template2;

			var subtotal1 = Number(document.getElementById("subtotal").innerHTML);
				subtotal1 = Math.round(subtotal1 + price);
				subtotal = subtotal1;

			document.getElementById("subtotal").innerHTML = subtotal1;
			document.getElementById("subtotal2").innerHTML = subtotal1;

            document.getElementById("btnIraDetalles").style.display = 'block';
            document.getElementById("btnIraPagos").style.display = 'block';

			calcularIva();
			funcCalcularEnvio();
	    	funcCalcularPago();
	    	sumaTotal();
	}
}

function sumaTotal(){
	total = subtotal + iva + gastosEnvio + tipoPago;
	document.getElementById('total').innerHTML = total.toFixed(2);
	document.getElementById('total2').innerHTML = total.toFixed(2);
}

function calcularIva(){
	iva = Math.round(subtotal * 0.12);
	document.getElementById("iva").innerHTML = iva.toFixed(2);
	document.getElementById("iva2").innerHTML = iva.toFixed(2);
}


function deleteItem(divToRemove, modalEliminar, counterPhafer, price) {

	var subtotal1 = Number(document.getElementById("subtotal").innerHTML);
		subtotal1 = Math.round(subtotal1 - price);
		subtotal = subtotal1;

	document.getElementById("subtotal").innerHTML = subtotal1.toFixed(2);

	var counter = Number(document.getElementById("items_count").innerHTML) - 1;
	document.getElementById("items_count").innerHTML = counter

    if(counter <= 0) {
        document.getElementById("btnIraDetalles").style.display = 'none';
        document.getElementById("btnIraPagos").style.display = 'none';
    }

	document.getElementById(divToRemove).remove();
	document.getElementById(modalEliminar).remove();

	var counterPadre = Number(document.getElementById("count_"+counterPhafer).innerHTML);
	document.getElementById("count_"+counterPhafer).innerHTML = counterPadre + 1;

	calcularIva();
	funcCalcularEnvio();
	funcCalcularPago();
	sumaTotal();
}

function funcCalcularEnvio() {
    console.log("subtotal: " + subtotal);
    console.log("porcentajeEnvio: " + porcentajeEnvio);

	gastosEnvio = (porcentajeEnvio * subtotal) / 100;

	document.getElementById('costoEnvio').innerHTML = gastosEnvio.toFixed(2);
	document.getElementById('costoEnvio2').innerHTML = gastosEnvio.toFixed(2);
}

function funcCalcularPago() {
    console.log("subtotal: " + subtotal);
    console.log("porcentajePago: " + porcentajePago);

	tipoPago = (porcentajePago * subtotal) / 100;

	document.getElementById('metodoPago').innerHTML = tipoPago.toFixed(2);
	document.getElementById('metodoPago2').innerHTML = tipoPago.toFixed(2);
}


document.getElementById("destinoEnvio").onchange = function() {calcularEnvio()};
function calcularEnvio() {
    porcentajeEnvio = Number(document.getElementById("destinoEnvio").value);

    if(porcentajeEnvio == ""){
        selectedEnvio = false;
        document.getElementById('costoEnvio').innerHTML = "00.00";
        document.getElementById('costoEnvio2').innerHTML = "00.00";
    } else {
    	funcCalcularEnvio();
    	sumaTotal();
    	selectedEnvio = true
        /* 
        if(subTotalOrden > 0 ){
            totalEnvio = (subTotalOrden * impuestoEnvio) / 100;
            document.getElementById('total-envio').innerHTML = totalEnvio.toFixed(2);
        } else {
            document.getElementById('total-envio').innerHTML = "00.00";
        }
        totalPagar();
        */
    }
}

document.getElementById("tipoPago").onchange = function() {calcularPago()};
function calcularPago() {
    porcentajePago = Number(document.getElementById("tipoPago").value);

    if(porcentajePago == ""){
        selectedPago = false;
        document.getElementById('metodoPago2').innerHTML = "00.00";
        document.getElementById('metodoPago').innerHTML = "00.00";
    } else {
    	funcCalcularPago();
    	sumaTotal();
    	selectedPago = true
        /* 
        if(subTotalOrden > 0 ){
            totalEnvio = (subTotalOrden * impuestoEnvio) / 100;
            document.getElementById('total-envio').innerHTML = totalEnvio.toFixed(2);
        } else {
            document.getElementById('total-envio').innerHTML = "00.00";
        }
        totalPagar();
        */
    }
}

function detallesVenta(){

	var scopeDiv = '_d';
	document.getElementById('detalle-compra').innerHTML += '<div id="'+scopeDiv+'"></div>';
    document.getElementById(scopeDiv).innerHTML = "";
    document.getElementById(scopeDiv).innerHTML += '<p>Nombre cliente: <strong>'+ nombreCliente +'</strong></p>';
    document.getElementById(scopeDiv).innerHTML += '<p>Nit cliente: <strong>'+ nitCliente +'</strong></p>';
    document.getElementById(scopeDiv).innerHTML += '<p>Total a pagar: <strong>'+ total +'</strong></p>';
    document.getElementById(scopeDiv).innerHTML += '<p>Posible fecha entrega: <strong>'+ fechaEntrega +'</strong></p>';
    document.getElementById(scopeDiv).innerHTML += '<p><small> Las fechas de entrega son en días hábiles.</small></p>';
}


function validarForm()
{
	nombreCliente = document.getElementById("nombre").value;
	nitCliente = document.getElementById("nit").value;

	if(nitCliente == ""){
		alert("Favor de ingresar su nombre");
		return false;
	}

	if(nit == ""){
		alert("Favor de ingresar su nit");
		return false;
	}

	if(!selectedPago) {
		alert("Seleccione un metodo de pago");
		return false;
	}

	if(!selectedEnvio) {
		alert("seleccione uno de los destinos de envios");
		return false;
	}

	detallesVenta();
}

