<?php 
	require_once('initialize.php');

	session_start();

	$breadcrumd = ['Inicio' => "index.php", 'Crear Personaje' => ""];
	$poderInicial = ((100 + 500) / 2 ) * 2.5;

	$players = getAll('players');


	if(isset($_POST['token']) && $_POST["token"] != "") 
	{
		//$playerResult = createItem('players', ['name' => $_POST['name']]);
	    $subject = [];
	    $subject['name'] = $_POST['name'];

		$values = [
			'nivel' => $_POST['nivel'],
			'ataque' => $_POST['ataque'],
			'defensa' => $_POST['defensa'],
			'rapidez' => $_POST['rapidez'],
			'poder_total' => $_POST['poder_total']
		];

	    $playerResult = insert_player($subject, $values);

	    if($playerResult === true){

	    	$player1 = getByName('players', $_POST['name']);

			if(!is_null($player1)) {
				$player = mysqli_fetch_assoc($player1);

				$values['player_id'] = $player['id'];
				$playerResult2 = insert_player_levels($values);

				if($playerResult2 === true) {

					header("Location: play.php");
				}

				$_SESSION["id"] = $player['id'];
				$_SESSION["nivel"] = $_POST['nivel'];
			}
	    } else {
	        $errors = $playerResult;
	    }

	}

	echo view("header", ["page_title" => "Crear Personaje", "breadcrumd" => $breadcrumd] );
 ?>

<!--ID (generado automáticamente)
o Nombre
o Nivel (1 inicial)
o Ataque (100 inicial)
o Defensa (500 incial)
o Rápidez (250 inicial)
o Poder Total (Ataque + Defensa / 2 x 2.5) -->

<div class="container">
	<div class="row">
		<div class="col-md-8">
			<div class="card card-primary">
			  <div class="card-header"> Formulario</div>
			  <div class="card-body">
				<form id="formParcial1" action="" method="post">

					 <?php echo display_errors($errors); ?>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Nombre:</label>
						        <input type="text" class="form-control" name="name" id="name">
						    </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Nivel:</label>
						        <input type="number"  min="0" max="100" class="form-control" name="nivel" id="nivel" value="1" readonly>
						    </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Ataque:</label>
						        <input type="number"  min="0" max="100" class="form-control" name="ataque" id="ataque" value="100" readonly>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Defensa:</label>
						        <input type="number"  min="0" max="100" class="form-control" name="defensa" id="defensa" value="500" readonly>
						    </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Rápidez:</label>
						        <input type="number"  min="0" max="100" class="form-control" name="rapidez" id="rapidez" value="250" readonly>
						    </div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
						        <label for="name">Poder Total:</label>
						        <input type="number"  min="0" max="100" class="form-control" name="poder_total" id="poder_total" value="<?php echo $poderInicial; ?>" readonly>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="token" value="<?php echo md5("holamundo"); ?>">
				    		<button type="submit" class="btn btn-default">Submit</button>	
						</div>
					</div>
				</form>
			  </div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="card card-primary">
			  <div class="card-header"> Lista de bots</div>
			  <div class="card-body">
					<ul>
	                    <?php while($subject = mysqli_fetch_assoc($players)) { ?>
	                    	<li><a href="play.php?id=<?php echo $subject['id']; ?>"><?php echo $subject['name']; ?></a></li>
	                    <?php } ?>
					</ul>
			  </div>
			</div>
		</div>
	</div>
</div>
   

<?php echo view("footer", []); ?>