<?php 
	require_once('initialize.php');

	$breadcrumd = ['Inicio' => "index.php", 'Tarea 1' => ""];

	echo view("header", ["page_title" => "Tarea 1", "breadcrumd" => $breadcrumd] );
 ?>


<div class="container">
	<div class="row">
		<div class="col-md-6">
			<div class="card card-primary">
			  <div class="card-header"> Game Cube</div>
			  <div class="card-body">
				  	<p><img src="https://upload.wikimedia.org/wikipedia/commons/thumb/d/d1/GameCube-Set.jpg/1024px-GameCube-Set.jpg"> </p>

				  	<p>The GameCube[b] is a home video game console released by Nintendo in Japan and North America in 2001 and Europe and Australia in 2002. The sixth generation console is the successor to the Nintendo 64 and competed with Sony's PlayStation 2 and Microsoft's Xbox.</p>

					<h3>Q 350.00</h3>

					<form action="details.php" method="get">
                        <p class="control is-expanded">
                             <input class="input is-rounded is-info" type="number" value="1" placeholder="0" min="1" name="cantidad">
                        </p>
                        <p class="control">
                             <input class="button is-info" type="submit" value="Agregar">
                        </p>
                  	</form>


			  </div>
			</div>
		</div>

		<p>&nbsp;</p>

	</div>
</div>
  
<?php echo view("footer", []); ?>