<?php 
	require_once('initialize.php');

	session_start();

	$breadcrumd = ['Inicio' => "index.php", 'Control de notas' => ""];

	$cursos = ['Matemáticas', 'Estadistica', 'Procesos económico'];

	if(isset($_POST['token']) && $_POST["token"] != "") {
		if(!isset($_SESSION["notas"])){
			$_SESSION["notas"] = [];
		}

		if($_POST["primerTrimestre"] != "" && $_POST["segundoTrimestre"] != "" && $_POST["tercerTrimestre"] != "" && $_POST["cuartoTrimestre"] != "") {
			$_POST["promedio"] = ($_POST["primerTrimestre"] + $_POST["segundoTrimestre"] + $_POST["tercerTrimestre"] + $_POST["cuartoTrimestre"]) / 4;
			array_push($_SESSION["notas"], $_POST);
			//$_SESSION["notas"] = [];
		}

	}

	echo view("header", ["page_title" => "Control de notas", "breadcrumd" => $breadcrumd] );
 ?>


<div class="container">
	<div class="row">
		<div class="col-md-12">
			<div class="card card-primary">
			  <div class="card-header"> Control de notas</div>
			  <div class="card-body">
				<form id="formParcial1" action="" method="post">

					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						        <label for="name">Nombre:</label>
						        <input type="text" class="form-control" name="nombre" id="name">
						    </div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
						        <label for="curso">Curso:</label>
						        <select class="form-control" name="curso" id="curso">
						        	<option value=""> -- Seleccione una materia --</option>
						        	<?php foreach ($cursos as $key1 => $curso) { ?>
						            	<option value="<?php echo $curso; ?>"><?php echo $curso; ?></option>
						        	<?php } ?>            
						        </select>
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-3">
							<div class="form-group">
						        <label for="primerTrimestre">1er Trimestre:</label>
						        <input type="number" min="0" max="100" step="1" class="form-control" name="primerTrimestre" id="primerTrimestre">
						    </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
						        <label for="segundoTrimestre">2d Trimestre:</label>
						        <input type="number" min="0" max="100" step="1" class="form-control" name="segundoTrimestre" id="segundoTrimestre">
						    </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
						        <label for="tercerTrimestre">3er Trimestre:</label>
						        <input type="number" min="0" max="100" step="1" class="form-control" name="tercerTrimestre" id="tercerTrimestre">
						    </div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
						        <label for="cuartoTrimestre">4to Trimestre:</label>
						        <input type="number" min="0" max="100" step="1" class="form-control" name="cuartoTrimestre" id="cuartoTrimestre">
						    </div>
						</div>
					</div>

					<div class="row">
						<div class="col-md-6">
							<input type="hidden" name="token" value="<?php echo md5("holamundo"); ?>">
				    		<button type="submit" class="btn btn-default">Submit</button>	
						</div>
					</div>
				</form>
			  </div>
			</div>
		</div>

		<p>&nbsp;</p>

		<?php if (isset($_POST['token']) && !is_null($_POST['token'])) {

			$errorCount = 0;
			foreach ($_POST as $key => $valuePost) {
				if($valuePost == ""){
					$errorCount++;
				}
			}

			if($errorCount>0) { ?>
			 	<div class="col-md-12">
					<div class="alert alert-danger">
					  <strong>Danger!</strong> Error en el formulario
					</div>
				</div>
		 	<?php } 

		 } ?>

	 	<?php if( !empty($_SESSION["notas"])) { ?>
			<div class="col-md-12">
				<div class="card card-primary">
					<div class="card-header"> Notas Obtenidas</div>
					<div class="card-body">
						<table class="table">
							<thead>
								<tr>
								<th>Alumno</th>
								<th>Materia</th>
								<th>Primers Trimestre</th>
								<th>Segudo Trimestre</th>
								<th>Tercer Trimestre</th>
								<th>Cuarto Trimestre</th>
								<th>Promedio</th>
								<th>Action</th>
							</tr>
							</thead>
						    <tbody id="tableResultado">
						    	<?php foreach ($_SESSION["notas"] as $key => $nota) { ?>
						    		<tr>
						    			<td><?php echo $nota['nombre']; ?></td>
						    			<td><?php echo $nota['curso']; ?></td>
						    			<td><?php echo $nota['primerTrimestre']; ?></td>
						    			<td><?php echo $nota['segundoTrimestre']; ?></td>
						    			<td><?php echo $nota['tercerTrimestre']; ?></td>
						    			<td><?php echo $nota['cuartoTrimestre']; ?></td>
						    			<td class="<?php // echo $nota['promedio'] >= 70 ? "btn-success" : "btn-danger" ?>"><?php echo $nota['promedio']; ?></td>
						    			<td>
						    				<a  class="btn btn-primary" href="mostrarnota.php?notaid=<?php echo $key; ?>">Ver nota</a>
						    			</td>
						    		</tr>
						    	<?php } ?>
						    </tbody>
						</table>
					</div>
				</div>
			</div>
	 	<?php } ?>

	</div>
</div>
   

<?php echo view("footer", []); ?>